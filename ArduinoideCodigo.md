#include <ArduinoJson.h>
#include <IOXhop_FirebaseESP32.h>
#include <WiFi.h>
/* musica1 do L; musica 2 zelda; musica 3 mario; */
#define WIFI_SSID "BLUE"
#define WIFI_PASSWORD "2444666668888888"
#define FIREBASE_HOST "https://teste-5b995-default-rtdb.firebaseio.com/"
#define FIREBASE_AUTH "4swl2kCXpS4nwQGBhUEmvOLpzDnw5vetyH5JRGzq"
const int La0=220, Sib0=233, Si0=247, Do0=261.5, Reb0=277, Re0=293.5, Mib0=311, Mi0=330, Fa0=349, Solb0=370, Sol0=392, Lab0=415, La1=440, Sib1=466, si1=494, Do1=523, Reb1=554, Re1=587, Mib1=622, Mi1=659, Fa1=698, Solb1=740, Sol1=784, Lab1=830;
int musica = 0;
int maxmax;
int minmin;
int planta;
const int aguinha = 35;
const int som = 25;


void setup() {
  Serial.begin(115200);

  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  Serial.print("Conectando ao wifi");
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(500);
  }

    Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
  Serial.println();
  
  pinMode(aguinha, INPUT);
  pinMode(som , OUTPUT);
  
  Serial.println("Setup complete");
}

void loop() {
  maxmax = (Firebase.getString("/Floresp/mmax")).toInt();
  minmin = (Firebase.getString("/Floresp/mmin")).toInt();
  musica = (Firebase.getString("/Floresp/Toque")).toInt();
  planta = (Firebase.getString("/Floresp/Planta")).toInt();
  double pct = (4095 - (analogRead(aguinha))) / 40.95;
  Serial.println(maxmax);
  delay(300);
  Serial.println(minmin);
  delay(300);
  Serial.println(pct);
  delay(2024);
  if(pct < minmin) {                                                                 
    if (musica == 2 ) {
      tone(som , Re0);
      delay(200);
      noTone(som);
      delay(0);
      tone(som , Fa0);
      delay(240);
      noTone(som);
      delay(0);
      tone(som , Re1);
      delay(800);
      noTone(som);
      delay(100);
      Serial.println(maxmax);
      Serial.println(minmin);
      Serial.println(pct);
      tone(som , Re0);
      delay(210);
      noTone(som);
      delay(0);
      tone(som , Fa0);
      delay(250);
      noTone(som);
      delay(0);
      tone(som , Re1);
      delay(800);
      noTone(som);
      delay(100);
      Serial.println(maxmax);
      Serial.println(minmin);
      Serial.println(pct);
      tone(som , Mi1);
      delay(600);
      noTone(som);
      delay(80);
      tone(som , Fa1);
      delay(230);
      noTone(som);
      delay(0);
      tone(som , Mi1);
      delay(200);
      noTone(som);
      delay(0);
      tone(som , Fa1);
      delay(210);
      noTone(som);
      delay(0);
      tone(som , Mi1);
      delay(190);
      noTone(som);
      delay(0);
      tone(som , Do1);
      delay(210);
      noTone(som);
      delay(0);
      tone(som , La1);
      delay(900);
      noTone(som);
      delay(90);
      Serial.println(maxmax);
      Serial.println(minmin);
      Serial.println(pct);
      tone(som , La1);
      delay(400);
      noTone(som);
      delay(50);
      tone(som , Re0);
      delay(400);
      noTone(som);
      delay(50);
      tone(som , Fa0);
      delay(200);
      noTone(som);
      delay(0);
      tone(som , Sol0);
      delay(230);
      noTone(som);
      delay(0);
      tone(som , La1);
      delay(1000);
      noTone(som);
      delay(350);
      Serial.println(maxmax);
      Serial.println(minmin);
      Serial.println(pct);
      tone(som , La1);
      delay(450);
      noTone(som);
      delay(0);
      tone(som , Re0);
      delay(450);
      noTone(som);
      delay(0);
      tone(som , Fa0);
      delay(190);
      noTone(som);
      delay(0);
      tone(som , Sol0);
      delay(230);
      noTone(som);
      delay(0);
      tone(som , Mi0);
      delay(1000);
      noTone(som);
      delay(350);
    }
    else if (musica == 1){

      tone(som , 392);
      delay(100);
      noTone(som);
      delay(100);
      tone(som , 330);
      delay(100);
      noTone(som);
      delay(100);
      tone(som , 440);
      delay(100);
      noTone(som);
      delay(100);
      tone(som , 330);
      delay(100);
      noTone(som);
      delay(100);
      tone(som , 370);
      delay(100);
      noTone(som);
      delay(100);
      tone(som , 392);
      delay(100);
      noTone(som);
      delay(100);
      tone(som , 330);
      delay(100);
      noTone(som);
      delay(100);
      tone(som , 494);
      delay(100);
      noTone(som);
      delay(100);
      tone(som , 440);
      delay(100);
      noTone(som);
      delay(100);
      tone(som , 392);
      delay(100);
      noTone(som);
      delay(100);
      tone(som , 370);
      delay(100);
      noTone(som);
      delay(100);
      tone(som , 330);
      delay(100);
      noTone(som);
      delay(100);
      tone(som , 294);
      delay(100);
      noTone(som);
      delay(100);
      Serial.println(maxmax);
      Serial.println(minmin);
      Serial.println(pct);
      tone(som , 392);
      delay(100);
      noTone(som);
      delay(100);
      tone(som , 330);
      delay(100);
      noTone(som);
      delay(100);
      tone(som , 440);
      delay(100);
      noTone(som);
      delay(100);
      tone(som , 330);
      delay(100);
      noTone(som);
      delay(100);
      tone(som , 370);
      delay(100);
      noTone(som);
      delay(100);
      tone(som , 392);
      delay(100);
      noTone(som);
      delay(100);
      tone(som , 330);
      delay(100);
      noTone(som);
      delay(100);
      tone(som , 494);
      delay(100);
      noTone(som);
      delay(100);
      tone(som , 440);
      delay(100);
      noTone(som);
      delay(100);
      tone(som , 392);
      delay(100);
      noTone(som);
      delay(100);
      tone(som , 370);
      delay(100);
      noTone(som);
      delay(100);
      tone(som , 330);
      delay(100);
      noTone(som);
      delay(100);
      tone(som , 294);
      delay(100);
      noTone(som);
      delay(100);
      tone(som , 330);
      delay(100);
      noTone(som);
      delay(100);
    }
    else if (musica == 3){

      tone(som , Sol0);
      delay(400);
      noTone(som);
      delay(50);
      tone(som , Sol0);
      delay(200);
      noTone(som);
      delay(50);
      tone(som , Mi0);
      delay(200);
      noTone(som);
      delay(50);
      tone(som , Mi0);
      delay(400);
      noTone(som);
      delay(50);
      tone(som , Mi0);
      delay(200);
      noTone(som);
      delay(50);
      tone(som , Re0);
      delay(200);
      noTone(som);
      delay(50);
      tone(som , Sol0);
      delay(400);
      noTone(som);
      delay(50);
      tone(som , Sol0);
      delay(200);
      noTone(som);
      delay(50);
      tone(som , Mi0);
      delay(200);
      noTone(som);
      delay(50);
      tone(som , Mi0);
      delay(200);
      noTone(som);
      delay(50);
      tone(som , Mi0);
      delay(200);
      noTone(som);
      delay(50);
      tone(som , Re0);
      delay(200);
      noTone(som);
      delay(50);
      tone(som , Sol0);
      delay(400);
      noTone(som);
      delay(50);
      tone(som , Sol0);
      delay(400);
      noTone(som);
      delay(50);
      Serial.println(maxmax);
      Serial.println(minmin);
      Serial.println(pct);
      tone(som , Mi0);
      delay(150);
      noTone(som);
      delay(50);
      tone(som , Mi0);
      delay(400);
      noTone(som);
      delay(50);
      tone(som , Mi0);
      delay(200);
      noTone(som);
      delay(50);
      tone(som , Re0);
      delay(200);
      noTone(som);
      delay(50);
      tone(som , Sol0);
      delay(400);
      noTone(som);
      delay(50);
      tone(som , Sol0);
      delay(200);
      noTone(som);
      delay(50);
      tone(som , Mi0);
      delay(200);
      noTone(som);
      delay(50);
      tone(som , Mi0);
      delay(200);
      noTone(som);
      delay(50);
      tone(som , Mi0);
      delay(200);
      noTone(som);
      delay(50);
      tone(som , Re0);
      delay(200);
      noTone(som);
      delay(50);
      tone(som , Fa0);
      delay(400);
      noTone(som);
      delay(50);
      tone(som , Fa0);
      delay(400);
      noTone(som);
      delay(50);
      Serial.println(maxmax);
      Serial.println(minmin);
      Serial.println(pct);
      tone(som , Re0);
      delay(400);
      noTone(som);
      delay(50);
      tone(som , Re0);
      delay(200);
      noTone(som);
      delay(50);
      tone(som , Re0);
      delay(400);
      noTone(som);
      delay(50);
      tone(som , Do0);
      delay(200);
      noTone(som);
      delay(50);
      tone(som , Mi0);
      delay(200);
      noTone(som);
      delay(50);
      tone(som , Mi0);
      delay(400);
      noTone(som);
      delay(50);
      tone(som , Do0);
      delay(150);
      noTone(som);
      delay(50);
      tone(som , Do0);
      delay(200);
      noTone(som);
      delay(50);
      tone(som , Do0);
      delay(200);
      noTone(som);
      delay(50);
      tone(som , Sib0);
      delay(350);
      noTone(som);
      delay(50);
      tone(som , Do0);
      delay(400);
      noTone(som);
      delay(50);
      
    }
    else if (musica == 0) {
      tone(som , 0);
      delay(5000);
    }
  }
  if(pct > maxmax){
    tone(som , 370);
    delay(710);
    noTone(som);
    delay(0);
    tone(som , 294);
    delay(910);
    noTone(som);
    delay(0);
    tone(som , 294);
    delay(150);
    noTone(som);
    delay(0);
    tone(som , 330);
    delay(150);
    noTone(som);
    delay(0);
    tone(som , 349);
    delay(360);
    Serial.println(maxmax);
    Serial.println(minmin);
    Serial.println(pct);
    noTone(som);
    delay(0);
    tone(som , 330);
    delay(280);
    noTone(som);
    delay(0);
    tone(som , 294);
    delay(280);
    noTone(som);
    delay(0);
    tone(som , 277);
    delay(350);
    noTone(som);
    delay(0);
    tone(som , 294);
    delay(300);
    noTone(som);
    delay(0);
    tone(som , 330);
    delay(250);
    noTone(som);
    delay(0);
    tone(som , 370);
    delay(670);
    noTone(som);
    delay(0);
    tone(som , 494);
    delay(590);
    Serial.println(maxmax);
    Serial.println(minmin);
    Serial.println(pct);
    noTone(som);
    delay(0);
    tone(som , 247);
    delay(230);
    noTone(som);
    delay(0);
    tone(som , 277);
    delay(240);
    noTone(som);
    delay(0);
    tone(som , 294);
    delay(420);
    noTone(som);
    delay(0);
    tone(som , 330);
    delay(310);
    noTone(som);
    delay(0);
    tone(som , 294);
    delay(260);
    noTone(som);
    delay(0);
    tone(som , 277);
    delay(310);
    Serial.println(maxmax);
    Serial.println(minmin);
    Serial.println(pct);
    noTone(som);
    delay(0);
    tone(som , 440);
    delay(280);
    noTone(som);
    delay(0);
    tone(som , 392);
    delay(250);
    noTone(som);
    delay(0);
    tone(som , 370);
    delay(640);
    noTone(som);
    delay(0);
    tone(som , 294);
    delay(800);
    Serial.println(maxmax);
    Serial.println(minmin);
    Serial.println(pct);
    noTone(som);
    delay(0);
    tone(som , 294);
    delay(150);
    noTone(som);
    delay(0);
    tone(som , 330);
    delay(160);
    noTone(som);
    delay(0);
    tone(som , 349);
    delay(320);
    noTone(som);
    delay(0);
    tone(som , 330);
    delay(310);
    noTone(som);
    delay(0);
    tone(som , 294);
    delay(260);
    noTone(som);
    delay(0);
    tone(som , 277);
    delay(350);
    noTone(som);
    delay(0);
    Serial.println(maxmax);
    Serial.println(minmin);
    Serial.println(pct);
    tone(som , 294);
    delay(330);
    noTone(som);
    delay(0);
    tone(som , 330);
    delay(230);
    noTone(som);
    delay(0);
    tone(som , 370);
    delay(640);
    noTone(som);
    delay(0);
    tone(som , 494);
    delay(590);
    noTone(som);
    delay(0);
    Serial.println(maxmax);
    Serial.println(minmin);
    Serial.println(pct);
    tone(som , 494);
    delay(200);
    noTone(som);
    delay(0);
    tone(som , 554);
    delay(240);
    noTone(som);
    delay(0);
    tone(som , 588);
    delay(350);
    noTone(som);
    delay(0);
    Serial.println(maxmax);
    Serial.println(minmin);
    Serial.println(pct);
    tone(som , 392);
    delay(260);
    noTone(som);
    delay(0);
    tone(som , 370);
    delay(260);
    noTone(som);
    delay(0);
    tone(som , 349);
    delay(360);
    noTone(som);
    delay(0);
    Serial.println(maxmax);
    Serial.println(minmin);
    Serial.println(pct);
    tone(som , 588);
    delay(300);
    noTone(som);
    delay(0);
    tone(som , 440);
    delay(260);
    noTone(som);
    delay(0);
    tone(som , 494);
    delay(260);
    noTone(som);
    delay(0);   
  }
}
